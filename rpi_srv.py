#!/usr/bin/env python
# coding: utf8
from socket import *
from datetime import datetime
from time import strftime
import Adafruit_CharLCD as LCD


#Initialazing display
lcd_rs        = 18
lcd_en        = 23
lcd_d4        = 12
lcd_d5        = 16
lcd_d6        = 20
lcd_d7        = 21
lcd_backlight = 4
#-----------------
lcd_columns   = 16
lcd_lines     = 2
#-----------------
#lcd = LCD.Adafruit_CharLCD(lcd_rs,lcd_en, lcd_d4, lcd_d5,
#                           lcd_d6, lcd_d7, lcd_columns,
#                           lcd_lines, lcd_backlight)

lcd = LCD.Adafruit_CharLCDPlate()

if __name__=='__main__':
    HOST = '0.0.0.0'
    PORT = 21567
    BUFSIZ = 1024
    ADDR = (HOST, PORT)
    serversock = socket(AF_INET, SOCK_STREAM)
    serversock.bind(ADDR)
    serversock.listen(2)

    while 1:
        received_data = None
        #print 'waiting for connection...'
        clientsock, addr = serversock.accept()
        #print '...connected from:', addr
        data = clientsock.recv(BUFSIZ)
        received_data = data
        if  received_data:
            date = datetime.now().strftime('%b%d  %H:%M').center(16)
            if len(received_data)<16: received_data = received_data.center(16)
            #making string for two lines
            string_to_display = "{0}\n{1}".format(date, received_data)
            lcd.message(string_to_display)
            lcd.home()
            #clientsock.send(string_to_display)
            clientsock.close()
