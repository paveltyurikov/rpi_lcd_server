#-*- coding: utf-8 -*-
import urllib, json
from socket  import socket

try:
    url = "http://api.openweathermap.org/data/2.5/weather?q=Moscow&mode=json&appid=bf09f8b69ed05b58f0ed1a55f40a6710"
    response = urllib.urlopen(url)
except:
    response = None

#pressure deference from height on sea level
pressure_on_height_sea = 14

if response is not None:
    data = json.loads(response.read())
    temp = int(data['main']['temp'] - 273)
    if temp>=0: temp = "+{0}".format(temp)
    press = int(data['main']['pressure'])
    press = int(press * 0.75006375541921) - pressure_on_height_sea
    hum = data['main']['humidity']
    if int(hum)>99: hum = "99"
else:
    temp = 0
    hum =0
    press =0

weather = ("T:{0} H:{1} P:{2} \n").format(temp, hum, press)


sock = socket()
sock.connect(('0.0.0.0', 21567))
sock.send(weather)

data = sock.recv(1024)
sock.close()

print data
